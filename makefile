.PHONY: build clear

build :
	mkdir -p build
	cd build && cmake ./.. && make
	mv ./build/compile_commands.json ./compile_commands.json
clear:
	rm -rf ./build
	rm -rf ./debug
run:
	mpirun ./build/tp3
