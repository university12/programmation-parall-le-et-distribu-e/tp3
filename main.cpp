//
//  main.cpp
//

#include "Chrono.hpp"
#include "Matrix.hpp"

#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#define DEBUG_LOG false
#define SEQUENTIAL_COMPARISON true

using namespace std;

const int ROOT = 0;
const int SWAP_MSG_TAG = 0;

struct Pivot {
  double pivot_value;
  int owner_rank;
};

struct MpiParam {
  const int rank;
  const int number_of_process;
};

void invertSequential(Matrix &iA);
void invertParallel(Matrix &matrix_to_invert, const MpiParam &mpiParam);
std::pair<Pivot, size_t>
findLocalPivot(const MatrixConcatCols &matrix_concat_with_i, size_t current_row,
               const MpiParam &mpiParam);
bool isCurrentRankOwnerOfRow(const MpiParam &mpiParam, size_t i);
void reduceAndBroadcastGlobalPivot(const Pivot &local_pivot,
                                   Pivot &global_pivot);
void swapPivot(size_t current_row, const Pivot &global_pivot, size_t pivot_row,
               MatrixConcatCols &matrix_concat_with_i,
               const MpiParam &mpiParam);
void broadcastPivotedLine(MatrixConcatCols &matrix_concat_with_i,
                          size_t pivot_row, size_t current_row,
                          const Pivot &global_pivot, const MpiParam &mpiParam);
void elimination(MatrixConcatCols &matrix_concat_with_i, size_t current_row,
                 const MpiParam &mpiParam);
void rebuilding(Matrix &matrix_to_invert, const MpiParam &mpiParam,
                MatrixConcatCols &matrix_concat_with_i);
Matrix multiplyMatrix(const Matrix &iMat1, const Matrix &iMat2);
void log(const string &message, int endl_prefix, int endl_suffix);
bool currentProcessOwnsPivot(size_t current_row, const Pivot &global_pivot,
                             const MpiParam &mpiParam);
bool currentProcessOwnsRow(size_t current_row, const MpiParam &mpiParam);
bool currentProcessOwnsGlobalPivot(const Pivot &global_pivot,
                                   const MpiParam &mpiParam);
size_t calculateOwnerRankForRow(size_t row, const MpiParam &mpiParam);

ofstream FILE_STREAM;

int main(int argc, char **argv) {
  MPI::Init();
  const int rank = MPI::COMM_WORLD.Get_rank();
  const int number_of_process = MPI::COMM_WORLD.Get_size();
  const MpiParam mpiParam = {rank, number_of_process};

#if DEBUG_LOG
  cout << "opening debug file" << endl;
  string fileName = "./output" + to_string(rank) + ".txt";

  FILE_STREAM = ofstream(fileName);
  cout << "file is open : " << FILE_STREAM.is_open() << endl;

#endif

  unsigned int matrix_dimension = 7;
  std::vector<std::string> all_args;
  if (argc > 1) {
    all_args.assign(argv + 1, argv + argc);
    matrix_dimension = std::stoi(all_args[0]);
    if (rank == ROOT) {
      cout << "new matrix dimension : " << matrix_dimension << endl;
    }
  }

  MatrixRandom randomMatrix(matrix_dimension, matrix_dimension);
  Matrix matrix(randomMatrix);

  // put matrix in a data structure that we can broadcast
  double data[matrix_dimension * matrix_dimension];
  if (rank == ROOT) {
    std::copy(begin(matrix.getDataArray()), end(matrix.getDataArray()), data);
  }
  MPI::COMM_WORLD.Bcast(&data, matrix_dimension * matrix_dimension, MPI::DOUBLE,
                        ROOT);
  if (rank == ROOT) {
    cout << "Running " << number_of_process << " processes" << endl;
#if DEBUG_LOG
    cout << "Matrice random:\n" << matrix.str() << endl;
#endif
  } else {
    // if we are not on root, overwrite random matrix with root matrix
    const std::valarray<double> vdata(data,
                                      matrix_dimension * matrix_dimension);
    matrix = Matrix(matrix_dimension, matrix_dimension, vdata);
  }

  log("Matrice random" + to_string(rank) + ":\n" + matrix.str(), 1, 2);
#if SEQUENTIAL_COMPARISON
  if (rank == ROOT) {
    Matrix matrixSeqOriginal(matrix);
    Matrix matrixSeq(matrix);
    Chrono chronometer;
    invertSequential(matrixSeq);
    Matrix result = multiplyMatrix(matrixSeqOriginal, matrixSeq);

    cout << "SEQUENTIEL: Erreur: "
         << result.getDataArray().sum() - matrix_dimension << endl;
    double elapsed_time = chronometer.get();
    cout << "SEQUENTIEL: Le temps d'execution est de " << elapsed_time
         << " secondes" << endl;
    chronometer.pause();
  }

#endif

  double start = MPI::Wtime();
  invertParallel(matrix, mpiParam);
  double end = MPI::Wtime();
  MPI::Finalize();
  if (rank == ROOT) {
#if DEBUG_LOG
    cout << "Matrice inverse:\n" << matrix.str() << endl;
#endif
    Matrix lRes = multiplyMatrix(randomMatrix, matrix);

    cout << "Erreur: " << lRes.getDataArray().sum() - matrix_dimension << endl;
    cout << "Le temps d'execution est de " << end - start << " secondes"
         << endl;
  }

  FILE_STREAM.close();
  return 0;
}

void invertParallel(Matrix &matrix_to_invert, const MpiParam &mpiParam) {
  // vérifier que la matrice est carrée
  assert(matrix_to_invert.rows() == matrix_to_invert.cols());

  // construire la matrice [A I]
  MatrixConcatCols matrix_concat_with_i(
      matrix_to_invert, MatrixIdentity(matrix_to_invert.rows()));

  // traiter chaque rangée
  for (size_t current_row = 0; current_row < matrix_to_invert.rows();
       current_row++) {
    log("--------------------- ROW " + to_string(current_row) +
            "---------------------",
        1, 2);
    /* trouver le pivot le plus grand localement */
    std::pair<Pivot, size_t> pivot_position =
        findLocalPivot(matrix_concat_with_i, current_row, mpiParam);
    Pivot local_pivot = pivot_position.first;
    size_t pivot_row_number = pivot_position.second;

    log("ON " + to_string(mpiParam.rank) + " at find local pivot", 1, 1);
    log("found pivot at position : " + to_string(pivot_row_number) +
            " by owner rank " + to_string(local_pivot.owner_rank),
        0, 1);
    log(" with value " + to_string(local_pivot.pivot_value), 0, 2);

    Pivot global_pivot = {};
    reduceAndBroadcastGlobalPivot(local_pivot, global_pivot);
    log("I'm rank " + to_string(mpiParam.rank) +
            ", AFTER REDUCE/BCAST local pivot is " +
            to_string(local_pivot.pivot_value) + " at pos " +
            to_string(pivot_row_number),
        1, 1);
    log("AND global pivot is " + to_string(global_pivot.pivot_value) +
            " from rank owner " + to_string(global_pivot.owner_rank),
        0, 2);

    swapPivot(current_row, global_pivot, pivot_row_number, matrix_concat_with_i,
              mpiParam);
    broadcastPivotedLine(matrix_concat_with_i, pivot_row_number, current_row,
                         global_pivot, mpiParam);
    elimination(matrix_concat_with_i, current_row, mpiParam);
  }
  rebuilding(matrix_to_invert, mpiParam, matrix_concat_with_i);
}

void rebuilding(Matrix &matrix_to_invert, const MpiParam &mpiParam,
                MatrixConcatCols &matrix_concat_with_i) {

  log("--------------------- WE ARE RELOCATING TO PROCESS "
      "0---------------------",
      1, 2);
  // backward substitution-ish
  for (int current_row = matrix_to_invert.rows() - 1; current_row >= 0;
       current_row--) {
    log("current row " + to_string(current_row) + " out of " +
            to_string(matrix_to_invert.rows() - 1),
        3, 0);

    if (isCurrentRankOwnerOfRow(mpiParam, current_row)) {
      log("I AM " + to_string(mpiParam.rank) + " i own ROW " +
              to_string(current_row),
          2, 1);
      log("My matrix is :\n" + matrix_concat_with_i.str(), 0, 2);
      std::valarray<double> row_to_send =
          matrix_concat_with_i.getRowCopy(current_row);
      double to_send[matrix_concat_with_i.cols() + 1];
      std::copy(begin(row_to_send), end(row_to_send), to_send);
      to_send[matrix_concat_with_i.cols()] = current_row;
      if (mpiParam.rank != ROOT) {
        log("I'm sending my row to ROOT", 0, 2);

        MPI::COMM_WORLD.Send(&to_send, matrix_concat_with_i.cols() + 1,
                             MPI::DOUBLE, ROOT, SWAP_MSG_TAG);
        log("It's done now !", 0, 2);
      }
    }

    if (mpiParam.rank == ROOT) {

      int provenance_rank = calculateOwnerRankForRow(current_row, mpiParam);
      if (provenance_rank != 0) {
        log("computed provenance rank is : " + to_string(provenance_rank), 1,
            1);
        double to_receive[matrix_concat_with_i.cols() + 1];
        MPI::COMM_WORLD.Recv(&to_receive, matrix_concat_with_i.cols() + 1,
                             MPI::DOUBLE, provenance_rank, SWAP_MSG_TAG);
        double row_index = to_receive[matrix_concat_with_i.cols()];
        const std::valarray<double> row_receive(to_receive,
                                                matrix_concat_with_i.cols());
        log("replacing at row index : " + to_string(row_index), 1, 1);
        matrix_concat_with_i.setRow(row_receive, row_index);
        log("ROOT My matrix updated is :\n" + matrix_concat_with_i.str(), 1, 2);
      }
      matrix_to_invert
          .getRowSlice(current_row) = matrix_concat_with_i.getDataArray()[slice(
          current_row * matrix_concat_with_i.cols() + matrix_to_invert.cols(),
          matrix_to_invert.cols(), 1)];
    }
  }
  log("I AM " + to_string(mpiParam.rank) + " and I'm OUT", 2, 1);
}

size_t calculateOwnerRankForRow(size_t row, const MpiParam &mpiParam) {
  for (size_t rank = 0; rank < mpiParam.number_of_process; rank++) {
    if (rank == 0 && row == 1) {
      continue;
    }
    if (rank == row % mpiParam.number_of_process) {
      return rank;
    }
  }

  return 0;
}

std::pair<Pivot, size_t>
findLocalPivot(const MatrixConcatCols &matrix_concat_with_i, size_t current_row,
               const MpiParam &mpiParam) {
  double col_max_value = fabs(matrix_concat_with_i(current_row, current_row));
  int pivot_row_number = current_row;
  size_t i = 0;
  while (i % mpiParam.number_of_process != mpiParam.rank) {
    i++;
  }
  i -= 1;

  for (; i < matrix_concat_with_i.rows(); i += mpiParam.number_of_process) {
    if (fabs(matrix_concat_with_i(i, current_row)) > col_max_value &&
        isCurrentRankOwnerOfRow(mpiParam, i)) {
      log("I'm rank " + to_string(mpiParam.rank) + ", owner of row " +
              to_string(current_row),
          1, 1);
      col_max_value = fabs(matrix_concat_with_i(i, current_row));
      pivot_row_number = i;
    }
  }
  log("I'm rank " + to_string(mpiParam.rank) + ", local pivot is " +
          to_string(col_max_value) + " at pos " + to_string(pivot_row_number),
      1, 1);
  const Pivot local_pivot = {col_max_value, mpiParam.rank};
  return std::make_pair(local_pivot, pivot_row_number);
}

bool isCurrentRankOwnerOfRow(const MpiParam &mpiParam, size_t i) {
  if (mpiParam.number_of_process == 1) {
    return true;
  }
  if (mpiParam.rank != 0) {
    return mpiParam.rank == i % mpiParam.number_of_process;
  } else if (mpiParam.rank == 0 && i == 1) {
    return false;
  }
  return mpiParam.rank == i % mpiParam.number_of_process;
}

void reduceAndBroadcastGlobalPivot(const Pivot &local_pivot,
                                   Pivot &global_pivot) {
  MPI::COMM_WORLD.Reduce(&local_pivot, &global_pivot, 1, MPI::DOUBLE_INT,
                         MPI::MAXLOC, ROOT);
  MPI::COMM_WORLD.Bcast(&global_pivot, 1, MPI::DOUBLE_INT, ROOT);
}

void swapPivot(size_t current_row, const Pivot &global_pivot, size_t pivot_row,
               MatrixConcatCols &matrix_concat_with_i,
               const MpiParam &mpiParam) {

  if (currentProcessOwnsPivot(current_row, global_pivot, mpiParam)) {
    if (matrix_concat_with_i(pivot_row, current_row) == 0) {
      log("I'm rank " + to_string(mpiParam.rank) +
              " and I cannot invert matrix.",
          1, 1);
      throw runtime_error("Cannot invert matrix");
    }
    if (currentProcessOwnsRow(current_row, mpiParam)) {
      log("I'm rank " + to_string(mpiParam.rank) + " and I own row " +
              to_string(current_row) +
              " and i also own global pivot so I swap localy",
          1, 1);
      log("Swapping row : " + to_string(current_row), 0, 2);
      matrix_concat_with_i.swapRows(pivot_row, current_row);
    } else {
      log("I'm rank " + to_string(mpiParam.rank) + " and I DO NOT own row " +
              to_string(current_row) +
              " and the pivot and the row are at the same process",
          1, 1);
    }
  } else {
    if (currentProcessOwnsRow(
            current_row,
            mpiParam)) { // nous avons une ligne a echanger, il faut l'envoyer
      log("I'm rank " + to_string(mpiParam.rank) + " and I own row " +
              to_string(current_row) + " but i do not own global pivot",
          1, 1);
      log("SENDING row : " + to_string(current_row), 0, 2);
      std::valarray<double> row_to_send =
          matrix_concat_with_i.getRowCopy(current_row);
      double to_send[matrix_concat_with_i.cols()];
      std::copy(begin(row_to_send), end(row_to_send), to_send);
      MPI::COMM_WORLD.Send(&to_send, matrix_concat_with_i.cols(), MPI::DOUBLE,
                           global_pivot.owner_rank, SWAP_MSG_TAG);
    } else if (currentProcessOwnsGlobalPivot(
                   global_pivot, mpiParam)) { // nous devons avoir la ligne a
                                              // echanger, il faut la recevoir
      log("I'm rank " + to_string(mpiParam.rank) + " and I DO NOT own row " +
              to_string(current_row) + " but I own global pivot",
          1, 1);
      log("RECEIVING row : " + to_string(current_row), 0, 2);
      double to_receive[matrix_concat_with_i.cols()];
      MPI::COMM_WORLD.Recv(&to_receive, matrix_concat_with_i.cols(),
                           MPI::DOUBLE, MPI::ANY_SOURCE, SWAP_MSG_TAG);
      const std::valarray<double> row_receive(to_receive,
                                              matrix_concat_with_i.cols());
      matrix_concat_with_i.setRow(row_receive, current_row);
      matrix_concat_with_i.swapRows(pivot_row, current_row);
      log("My matrix updated is :\n" + matrix_concat_with_i.str(), 1, 2);
    } else {
      log("I'm rank " + to_string(mpiParam.rank) + " and I DO NOT own row " +
              to_string(current_row) + " and I'm not the pivot",
          1, 1);
    }
  }
}

bool currentProcessOwnsGlobalPivot(const Pivot &global_pivot,
                                   const MpiParam &mpiParam) {
  if (mpiParam.number_of_process == 1) {
    return true;
  }
  return global_pivot.owner_rank == mpiParam.rank;
}

bool currentProcessOwnsRow(size_t current_row, const MpiParam &mpiParam) {

  if (mpiParam.number_of_process == 1) {
    return true;
  }

  if (mpiParam.rank != 0) {
    return current_row % mpiParam.number_of_process == mpiParam.rank;
  } else if (mpiParam.rank == 0 && current_row == 1) {
    return false;
  }
  return current_row % mpiParam.number_of_process == mpiParam.rank;
}

bool currentProcessOwnsPivot(size_t current_row, const Pivot &global_pivot,
                             const MpiParam &mpiParam) {
  if (mpiParam.number_of_process == 1) {
    return true;
  }
  if (global_pivot.owner_rank != 0) {
    return current_row % mpiParam.number_of_process == global_pivot.owner_rank;
  } else if (global_pivot.owner_rank == 0 && current_row == 1) {
    return false;
  }
  return current_row % mpiParam.number_of_process == global_pivot.owner_rank;
}

void broadcastPivotedLine(MatrixConcatCols &matrix_concat_with_i,
                          size_t pivot_row, size_t current_row,
                          const Pivot &global_pivot, const MpiParam &mpiParam) {

  double to_broadcast[matrix_concat_with_i.cols()];
  bool edited = false;

  if (mpiParam.rank == global_pivot.owner_rank) {
    const double pivot_val = matrix_concat_with_i(current_row, current_row);
    for (size_t col = 0; col < matrix_concat_with_i.cols(); col++) {
      matrix_concat_with_i(current_row, col) /= pivot_val;
    }
    log("I'm rank " + to_string(mpiParam.rank) + " and I own pivot for row " +
            to_string(current_row) + "I'm gonna send it",
        1, 1);
    std::valarray<double> row_to_send =
        matrix_concat_with_i.getRowCopy(current_row);
    std::copy(begin(row_to_send), end(row_to_send), to_broadcast);
    edited = true;
    log("My matrix is :\n" + matrix_concat_with_i.str(), 1, 2);
  }

  log("I'm rank " + to_string(mpiParam.rank) +
          " And BROADCASTING row. just so you know, I edited the row ? : " +
          to_string(edited),
      1, 1);
  MPI::COMM_WORLD.Bcast(&to_broadcast, matrix_concat_with_i.cols(), MPI::DOUBLE,
                        global_pivot.owner_rank);
  if (mpiParam.rank != global_pivot.owner_rank) {
    const std::valarray<double> row_received(to_broadcast,
                                             matrix_concat_with_i.cols());
    matrix_concat_with_i.setRow(row_received, current_row);
  }
  log("My matrix updated is :\n" + matrix_concat_with_i.str(), 1, 2);
}

void elimination(MatrixConcatCols &matrix_concat_with_i, size_t current_row,
                 const MpiParam &mpiParam) {

  size_t row = 0;

  log("I am rank " + to_string(mpiParam.rank), 1, 1);
  while (row % mpiParam.number_of_process != mpiParam.rank) {
    row++;
  }
  log("I've normalize the pivot row", 1, 2);
  log("My matrix is :\n" + matrix_concat_with_i.str(), 1, 2);
  log("Starting elimination at row " + to_string(row), 0, 1);

  for (; row < matrix_concat_with_i.rows(); row += mpiParam.number_of_process) {
    if (row != current_row) {
      const double factor = matrix_concat_with_i(row, current_row);
      matrix_concat_with_i.getRowSlice(row) -=
          matrix_concat_with_i.getRowCopy(current_row) * factor;
    }
  }

  log("My matrix is :\n" + matrix_concat_with_i.str(), 1, 2);
  log("elimination done.", 0, 2);
}

// Multiplier deux matrices.
Matrix multiplyMatrix(const Matrix &iMat1, const Matrix &iMat2) {

  // vérifier la compatibilité des matrices
  assert(iMat1.cols() == iMat2.rows());
  // effectuer le produit matriciel
  Matrix lRes(iMat1.rows(), iMat2.cols());
  // traiter chaque rangée
  for (size_t i = 0; i < lRes.rows(); ++i) {
    // traiter chaque colonne
    for (size_t j = 0; j < lRes.cols(); ++j) {
      lRes(i, j) = (iMat1.getRowCopy(i) * iMat2.getColumnCopy(j)).sum();
    }
  }
  return lRes;
}

void log(const string &message, const int endl_prefix, const int endl_suffix) {
#if DEBUG_LOG
  if (endl_prefix > 0) {
    for (int i = 0; i < endl_prefix; i++) {
      FILE_STREAM << endl;
    }
  }
  FILE_STREAM << message;
  if (endl_suffix > 0) {
    for (int i = 0; i < endl_suffix; i++) {
      FILE_STREAM << endl;
    }
  }
#endif
}

void invertSequential(Matrix &iA) {

  // vérifier que la matrice est carrée
  assert(iA.rows() == iA.cols());
  // construire la matrice [A I]
  MatrixConcatCols lAI(iA, MatrixIdentity(iA.rows()));

  // traiter chaque rangée
  for (size_t k = 0; k < iA.rows(); ++k) {
    // trouver l'index p du plus grand pivot de la colonne k en valeur absolue
    // (pour une meilleure stabilité numérique).
    size_t p = k;
    double lMax = fabs(lAI(k, k));
    for (size_t i = k; i < lAI.rows(); ++i) {
      if (fabs(lAI(i, k)) > lMax) {
        lMax = fabs(lAI(i, k));
        p = i;
      }
    }
    // vérifier que la matrice n'est pas singulière
    if (lAI(p, k) == 0)
      throw runtime_error("Matrix not invertible");

    // échanger la ligne courante avec celle du pivot
    if (p != k)
      lAI.swapRows(p, k);

    double lValue = lAI(k, k);
    for (size_t j = 0; j < lAI.cols(); ++j) {
      // On divise les éléments de la rangée k
      // par la valeur du pivot.
      // Ainsi, lAI(k,k) deviendra égal à 1.
      lAI(k, j) /= lValue;
    }

    // Pour chaque rangée...
    for (size_t i = 0; i < lAI.rows(); ++i) {
      if (i != k) { // ...différente de k
        // On soustrait la rangée k
        // multipliée par l'élément k de la rangée courante
        double lValue = lAI(i, k);
        lAI.getRowSlice(i) -= lAI.getRowCopy(k) * lValue;
      }
    }
  }

  // On copie la partie droite de la matrice AI ainsi transformée
  // dans la matrice courante (this).
  for (unsigned int i = 0; i < iA.rows(); ++i) {
    iA.getRowSlice(i) =
        lAI.getDataArray()[slice(i * lAI.cols() + iA.cols(), iA.cols(), 1)];
  }
}